class CPUClass:
    ignoreinterupts=0 # DI will set to 1 to disable interupts

## Registries

    # 16 bit registers.
    reg_pc=0 # Program Counter Register  // Store as integer
    reg_sp=0 # Stack Pointer Register
    reg_ix=0 # Index Register 1
    reg_iy=0 # Index Register 2

    # Special registers.
    reg_a=0  # The Accumulator.
    reg_f={
        "S":0,
        "Z":0,
        "F5":0,
        "H":0,
        "F3":0,
        "P/V":0,
        "N":0,
        "C":0
    }
            # The flag register.
    reg_a2=0  # The Accumulator '.
    reg_f2={
        "S":0,
        "Z":0,
        "F5":0,
        "H":0,
        "F3":0,
        "P/V":0,
        "N":0,
        "C":0
    }  # The flag register '.


# I is the interrupt vector register. It is used by the calculator in the interrupt 2 mode.
# R is the refresh register. Although it holds no specific purpose to the OS, it can be used to generate random numbers.
    reg_i=0  # The I register.
    reg_r=0  # The R register

    # 8 bit registers -- Store as HEX
    reg_b=0  # The B register.
    reg_c=0  # The C register
    reg_d=0  # The D register
    reg_e=0  # The E register
    reg_h=0  # The H register
    reg_l=0  # The L register
    reg_w=0  # The W register
    reg_z=0  # The Z register

    reg_b2=0  # The B register '
    reg_c2=0  # The C register '
    reg_d2=0  # The D register '
    reg_e2=0  # The E register '
    reg_h2=0  # The H register '
    reg_l2=0  # The L register '
    reg_w2=0  # The W register '
    reg_z2=0  # The Z register '


## Main routine.
    def instruct(self, memory, interpretor, cpu, ports):
        # PC holds the memory location of the next instruction to run.
        print "Run instruction"
        print "Starting at: " + str(self.reg_pc)
        instruction = memory.get(self.reg_pc)
        print "Instruction:" + instruction
        self.reg_pc = self.reg_pc + 1

        # The extended set
        if (instruction == 'ed'):
            instruction = instruction + memory.get(self.reg_pc)
            self.reg_pc = self.reg_pc + 1

        print "Run instruction int_" + instruction

        myfunc = "int_" + instruction

        length = interpretor.myfunc(myfunc, cpu, memory, ports)

        print "I have run instruction"
        print "PC is at:" + str(self.reg_pc)

        return length
## Interupts handling.