class InterpretorClass:

#Main instructions

    def int_00(self, cpu, memory, ports):

          # NOP - N operation performed.

          return 4

    def int_01(self, cpu, memory, ports):

          # LD BC, NN
          cpu.reg_b = memory.get(cpu.reg_pc)
          cpu.reg_pc = cpu.reg_pc + 1
          cpu.reg_c = memory.get(cpu.reg_pc)
          cpu.reg_pc = cpu.reg_pc + 1

          return 10


    def int_11(self, cpu, memory, ports):

           # LD DE, NN

           # Get next two

           cpu.reg_d = memory.get(cpu.reg_pc)
           cpu.reg_pc = cpu.reg_pc + 1
           cpu.reg_e = memory.get(cpu.reg_pc)
           cpu.reg_pc = cpu.reg_pc + 1

           return 10

    def int_2b(self, cpu, memory, ports):

        # DEC HL
        tmp1 = cpu.reg_h
        tmp2 = cpu.reg_l

        value = int(tmp1 + tmp2, 16)
        value = value -1
        if (value == -1):
            value = 65535

        tmp1 = int(value / 256)
        tmp2 = value - (256 * tmp1)

        cpu.reg_h=format(tmp1, '02x')
        cpu.reg_l=format(tmp2, '02x')

        return 6

    def int_36(self, cpu, memory, ports):

           # ld (hl),*
            tmp1 = memory.get(cpu.reg_pc)
            cpu.reg_pc = cpu.reg_pc + 1
            memory.set(str(cpu.reg_h) + str(cpu.reg_l), str(tmp1))
            return 10


    def int_3e(self, cpu, memory, ports):

           # LD A, NN
           cpu.reg_a = memory.get(cpu.reg_pc)
           cpu.reg_pc = cpu.reg_pc + 1

           return 7

    def int_47(self, cpu, memory, ports):

           # LD B,A

           cpu.reg_b = cpu.reg_a

           return 4

    def int_62(self, cpu, memory, ports):

           # LD H, D

           cpu.reg_h = cpu.reg_d

           return 4

    def int_6b(self, cpu, memory, ports):

           # LD  L, E
           cpu.reg_l = cpu.reg_e

           return 4

    def int_af(self, cpu, memory, ports):

           # XOR A
           # XOR is an instruction that takes one 8-bit input and compares it with the accumulator.
           # XOR is similar to Or, except for one thing: only 1 of the 2 test bits can be set or else it will result in a zero.
           # The final answer is stored to the accumulator.
           # C and N flags cleared. P/V is parity, and rest are modified by definition.

           cpu.reg_a = 0
           cpu.reg_f={
                   "S":0,
                   "Z":0,
                   "F5":0,
                   "H":0,
                   "F3":0,
                   "P/V":0,
                   "N":0,
                   "C":0
               }

           return 4


    def int_c3(self, cpu, memory, ports):

            # JP NN
            tmp1 = memory.get(cpu.reg_pc)
            cpu.reg_pc = cpu.reg_pc + 1
            tmp2 = memory.get(cpu.reg_pc)
            cpu.reg_pc = int(tmp2 + tmp1, 16)

            return 10


    def int_d3(self, cpu, memory, ports):

            # OUT (*),a
            tmp1 = memory.get(cpu.reg_pc)
            cpu.reg_pc = cpu.reg_pc + 1
            ports.out(tmp1, cpu.reg_a)

            return 11

    def int_f3(self, cpu, memory, ports):

        # DI
        # The DI Disables the Interrupts (both mode 1 and mode 2).
        # Cause the CPU to ignore all interrupt requests by using the DI (Disable Interrupt) instruction.
        cpu.ignoreinterupts=1
        return 4

    ############## THE EXTENDED SET

    def int_ed47(self, cpu, memory, ports):

        ### LD i,a
        cpu.reg_i = cpu.reg_a

        return 9



    def myfunc(self, method_name, cpu, memory, ports):
        method = getattr(self, method_name)
        return method(cpu, memory, ports)