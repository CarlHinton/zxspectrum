class MemoryClass:

   memory = []
   binmem = []

   def __init__(self, screen):
       self.screen = screen

   def init(self, rom, rompos, size):
       i = 0
       while i < size:
           self.memory.append("00")
           self.binmem.append("000000000")
           i += 1

       ### Add the ROM

       i = 0
       while rom.get(i):
           hex=rom.get(i)
           self.memory[i+rompos] = hex
           self.binmem[i+rompos] = hextobin(hex)

           i+= 1

   def get(self, position):
       try:
           ret=self.memory[position]
       except:
           return ""
       else:
           return ret

   def set(self,positionhex, hex):

        position = int(positionhex, 16)
        self.memory[position] = hex
        self.binmem[position] = hextobin(hex)

        ### Inform the screen
        self.screen.needsupdate.append(position)

   def getbin(self, position):
       try:
           ret=self.binmem[position]
       except:
           return ""
       else:
           return ret


   def loadscr(self, position, filename):

       f= open(filename,"r")
       counter=0
       while True:
           c = f.read(1)
           if not c:
               break

           hex=(toHex(c).lower())
           self.memory[position + counter]= hex
           self.binmem[position + counter]= hextobin(hex)
           counter=counter+1
       f.close()


def toHex(char):
    return char.encode("hex")

def hextobin(hex):
    scale = 16 ## equals to hexadecimal
    num_of_bits = 8
    return bin(int(hex, scale))[2:].zfill(num_of_bits)