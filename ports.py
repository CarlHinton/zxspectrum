# Port #fe
# The lowest three bits specify the border colour;
# a zero in bit 3 activates the MIC output,
# whilst a one in bit 4 activates the EAR output and the internal speaker.
# However, the EAR and MIC sockets are connected only by resistors, so activating one activates the other;
# the EAR is generally used for output as it produces a louder sound. The upper two bits are unused.
class PortsClass:

    def __init__(self, screen, keyboard, colours):

        self.screen = screen
        self.keyboard = keyboard
        self.colours = colours


    def out(self, port, value):

        # Only really bothered about port 254
        if (port == "fe"):

            binout = hextobin(value)
            print binout
            col = binout[5] + binout[6] + binout[7]
            deccol = int(col, 2)
            print deccol
            self.screen.border(self.colours[deccol])

        # TO DO - Handle sound OUT.



     # TO DO IN



def hextobin(hex):
    scale = 16 ## equals to hexadecimal
    num_of_bits = 8
    return bin(int(hex, scale))[2:].zfill(num_of_bits)