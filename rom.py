class RomClass:

   rom = []
   def init(self):
       f= open("48.rom","r")
       while True:
           c = f.read(1)
           if not c:
             break
           self.rom.append(toHex(c).lower())
       f.close()
   def get(self, position):
       try:
           ret=self.rom[position]
       except:
           return ""
       else:
           return ret

def toHex(char):
    return char.encode("hex")
