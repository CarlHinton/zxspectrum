from graphics import *
import time

class ScreenClass:
    mag=3
    bordercol=0
    screen_image=Image(Point((128 + 5) * mag, (96 + 8) * mag), 'screen' + str(mag) + '.gif')
    win = GraphWin('ZX Spectrum', (256 + 20) * mag, (200 + 20) * mag) # give title and dimensions
    '''This the screen'''

    def __init__(self):

        self.needsupdate = []

    def init(self, colours):

        self.menu()
        self.border(colours[self.bordercol])

    def menu(self):

        aColor = color_rgb(128, 0, 255)
        line = Line(Point(0,(10*self.mag)), Point((256 +20) * self.mag, (10*self.mag)))
        line.setWidth(self.mag)
        line.setOutline(aColor)
        line.draw(self.win)


    def border(self, colour):

        aColor = color_rgb(colour[0], colour[1], colour[2])

        # Top
        rect = Rectangle(Point(0, (10*self.mag)), Point((256 +20) * self.mag , (20*self.mag)))
        rect.setFill(aColor)
        rect.setOutline(aColor)
        rect.draw(self.win)

        # Right
        rect = Rectangle(Point((10+256)*self.mag, (20*self.mag)), Point((20+256)*self.mag, (200 + 10)*self.mag) )
        rect.setFill(aColor)
        rect.setOutline(aColor)
        rect.draw(self.win)

        # Bottom
        rect = Rectangle(Point(0, (200 + 10) * self.mag), Point((256 +20) * self.mag, (200 + 20) * self.mag))
        rect.setFill(aColor)
        rect.setOutline(aColor)
        rect.draw(self.win)

        # Left
        rect = Rectangle(Point(0, (20*self.mag)), Point((10*self.mag), (200 + 10)*self.mag))
        rect.setFill(aColor)
        rect.setOutline(aColor)
        rect.draw(self.win)

    def fullupdate(self, memory, colours, startpoint=0):
        # Redraw takes 3 secs - cp 50x per second.
        if startpoint == 0:
            print "TO DO"
        start=time.time()
        print start
        mempoint = startpoint
        for section in range(0,3):
            for row in range(0,8):
                for rrow in range(0,8):
                    for ccol in range(0,32):
                        #startpoint + ccol + (32 * rrow) + (256 * row) + (section * 2048))
                        binary = memory.getbin(mempoint)
                        mempoint += 1

                        # Calculate colour
                        background=getbackcol(startpoint, ccol, rrow, section, colours, memory)
                        foreground=getforecol(startpoint, ccol, rrow, section, colours, memory)


                        for bit in range(0,8):
                            bitvalue = binary[bit]

                            xchord = (10 + bit + (ccol*8)) * self.mag
                            ychord = (20 + (8*rrow) + (row) + (section * 64)) *self.mag
                            if bitvalue == "0":
                                self.plot(xchord, ychord, background)
                            else:
                                self.plot(xchord, ychord, foreground)

        self.screen_image.undraw()
        self.screen_image.draw(self.win)
        self.border(colours[self.bordercol])
        self.menu()

        end=time.time()
        print end
        print end-start

    def update(self, memory, colours, startpoint):
        # Redraw every 50x per second - just update the new stuff.
        print "To do"

    def plot(self, xchord, ychord, colour):

        #rect = Rectangle(Point(xchord, ychord), Point(xchord + self.mag, ychord+self.mag))
        #rect.setFill(colour)
        #rect.setOutline(colour)
        #rect.draw(self.win)
        for y in range(0, self.mag):
            for x in range(0, self.mag):
                self.screen_image.setPixel(xchord + x, ychord + y, colour)


    def update(self):

        # Is there anything in self.needsupdate
        if (self.needsupdate == []):
            return

        # We have something.
        for val in self.needsupdate:
            if (val >= 16384 and val <= 23296):
                print "Screen update required"
                print "TODO"

        self.needsupdate = []

def hextobin(hex):

    scale = 16 ## equals to hexadecimal
    num_of_bits = 8
    return bin(int(hex, scale))[2:].zfill(num_of_bits)

def getbackcol(startpoint, col, row, section, colours, memory):
    colstart=startpoint+6144
    binary = memory.getbin(colstart + col + (row * 32) + (section * 256))
    backcolbin = binary[2] + binary[3] + binary[4]
    bright = int(binary[1])
    col = int(backcolbin, 2) + (8 * bright)
    return color_rgb(colours[col][0], colours[col][1], colours[col][2])

def getforecol(startpoint, col, row, section, colours, memory):
    colstart=startpoint+6144
    binary = memory.getbin(colstart + col + (row * 32) + (section * 256))
    backcolbin = binary[5] + binary[6] + binary[7]
    bright = int(binary[1])
    col = int(backcolbin, 2) + (8 * bright)
    return color_rgb(colours[col][0], colours[col][1], colours[col][2])

