import rom
import screen
import interpretor
import cpu
import tape
import sound
import memory
import keyboard
import joystick
import ports

import time

speed=0.1
clock=time.time()

def hexToRGB(hex):

    h = hex.lstrip('#')
    return tuple(int(h[i:i+2], 16) for i in (0, 2 ,4))

# Get all 16 colours defined.
colours = ['#000000', '#0000D7', '#D70000', '#D700D7', '#00D700', '#00D7D7', '#D7D700', '#D7D7D7',
   '#000000', '#0000FF', '#FF0000', '#FF00FF', '#00FF00', '#00FFFF', '#FFFF00', '#FFFFFF']

j=0
for colour in colours:
    colours[j] = hexToRGB(colours[j])
    j+=1
j=0


#Initialise everything
theScreen = screen.ScreenClass()
theScreen.init(colours)
theRom = rom.RomClass()
theRom.init()
theCPU = cpu.CPUClass()
theMemory = memory.MemoryClass(theScreen)
theMemory.init(theRom,0, 65536)
theInterpretor = interpretor.InterpretorClass()
theKeyboard=keyboard.KeyboardClass()
thePorts = ports.PortsClass(theScreen, theKeyboard, colours)


theScreen.bordercol=4
theScreen.border(colours[4])

theMemory.loadscr(16384,"Jetpac.scr")
theMemory.loadscr(32000,"AticAtac.scr")

#theScreen.fullupdate(theMemory, colours)
theScreen.fullupdate(theMemory, colours, 16384)
theScreen.fullupdate(theMemory, colours, 32000)

#Main loop
while True:
    clock=time.time()
    length = theCPU.instruct(theMemory, theInterpretor, theCPU, thePorts)

    print "PC:" + str(theCPU.reg_pc)

    # Screen update.
    theScreen.update()
    # Keeping time.
    while time.time() < (clock + (length * speed)):
        continue



